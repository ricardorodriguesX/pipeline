package pipeline;

public abstract class Pipe {
	
	public abstract void handle(Object content) throws Exception;
}
