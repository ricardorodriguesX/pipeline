package pipeline;

public class Pipeline <T>{
	
	private T t;
	
	public Pipeline<T> send(T t)
	{
		this.t = t;
		
		return this;
	}
	
	public Pipeline<T> through(Pipe ...pipes) throws Exception
	{
		for(Pipe pipe : pipes) {
			
			pipe.handle(t);
		}
		
		return this;
	}
	
	public T thenReturn()
	{
		return t;
	}
}
