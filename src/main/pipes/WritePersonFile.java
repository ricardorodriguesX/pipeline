package main.pipes;


import java.io.PrintWriter;

import main.Person;
import pipeline.Pipe;

public class WritePersonFile extends Pipe{
	
	@Override
	public void handle(Object content) throws Exception 
	{
		Person person = (Person) content;
		
		PrintWriter writer = new PrintWriter(person.hashCode()+".txt", "UTF-8");
		
		writer.println(person.toString());
		
		writer.close();

	}
}
