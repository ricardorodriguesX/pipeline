package main.pipes;

import java.io.PrintWriter;
import java.nio.charset.Charset;
import java.util.Random;

import main.Person;
import pipeline.Pipe;

public class UpdatePersonFile extends Pipe{

	@Override
	public void handle(Object content) throws Exception
	{
		Person person = (Person) content;
		
		PrintWriter writer = new PrintWriter(person.hashCode()+".txt", "UTF-8");
		
		byte[] array = new byte[7];
	    new Random().nextBytes(array);
	    
	    String generatedString = new String(array, Charset.forName("UTF-8"));
		
		person.setName(generatedString);
		
		writer.println(person.toString());
		
		writer.close();

	}
}
