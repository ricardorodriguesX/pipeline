package main;

import main.pipes.UpdatePersonFile;
import main.pipes.WritePersonFile;
import pipeline.Pipeline;

public class Main {

	public static void main(String[] args) throws Exception {
		Pipeline <Person>pipeline = new Pipeline<Person>();
		
		Person person = new Person(156, "TESTE", 15);
		
		person = pipeline.send(person)
			.through(
				new WritePersonFile(),
				new UpdatePersonFile()
			)
			.thenReturn();
		
		System.out.println(person);
	}

}
