package main;

public class Person {

	private int height;

	private String name;

	private int age;


	public int getHeight() {
		return height;
	}

	public void setHeight(int height) {
		this.height = height;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public Person() {

	}

	public Person(int height, String name, int age) {
		this.setHeight(height);
		this.setName(name);
		this.setAge(age);
	}

	public Person(Person pessoa) {
		this(pessoa.getHeight(), pessoa.getName(), pessoa.getAge());
	}


	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + age;
		result = prime * result + height;
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}

	@Override
	public String toString() {
		return "Person [height=" + height + ", name=" + name + ", age=" + age + "]";
	}
}
